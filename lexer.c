#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"


// YOU CAN ADD YOUR OWN FUNCTIONS, DECLARATIONS AND VARIABLES HERE

FILE* fp;
//char file_name[100];
char global_file_name[100];
char wholeFile[9999];
int cp;
int linenumber;
char reswords[22][12] = {"class","constructor","method","function","int","boolean","char","void","var","static","field","let","do","if","else","while","return","true","false","null","this"};
char Symbols[20] = {'(', ')', '{', '}', '[', ']', '+', '-', '<', '>',';', '=', '.', ',', '/', '*', '&', '|', '~'};
// IMPLEMENT THE FOLLOWING functions
//***********************************

// Initialise the lexer to read from source file
// file_name is the name of the source file
// This requires opening the file and making any necessary initialisations of the lexer
// If an error occurs, the function should return 0
// if everything goes well the function should return 1
int InitLexer (char* file_name)
{
	linenumber = 1;
	cp = 0;
	strcpy(global_file_name,file_name);
	fp = fopen(file_name, "r");

	if (fp == NULL)
	{
		printf("Error: could not open file %s", file_name);
		return 0;
	}
	char ch;
	int character = 0;
	memset(wholeFile,0,strlen(wholeFile));
	while ((ch = fgetc(fp)) != EOF){
		wholeFile[character] = ch;
		character++;
	}
	wholeFile[character] = ch;
	//ch = fgetc(fp);
	//character++;
	//wholeFile[character] = ch;
	return 1;
}


// Get the next token from the source file
Token GetNextToken ()
{
	Token t;
	int tokenComplete = 0;
	int tc = 0;
	while(tokenComplete == 0){
		strcpy(t.fl,global_file_name);
		//clear the tokens string
		memset(t.lx,0,128);
		//checks for /*
		if(wholeFile[cp] == '/' && wholeFile[cp + 1] == '*'){
			while (wholeFile[cp] != '*' || wholeFile[cp + 1] != '/'){
				if(wholeFile[cp] == EOF){
					t.ec = EofInCom;
					strcpy(t.lx,"Error: unexpected eof in comment");
					t.ln = linenumber;
					t.tp = ERR;
					return t;
				}
				if(wholeFile[cp] == '\n'){
					linenumber++;
				}
				cp++;
			}
			cp++;cp++;
			continue;
		}
		//checks for //
		if(wholeFile[cp] == '/' && wholeFile[cp + 1] == '/'){
			while(wholeFile[cp] != '\n'){
				if(wholeFile[cp] == EOF){
					t.ec = EofInCom;
					strcpy(t.lx,"Error: unexpected eof in comment");
					t.ln = linenumber;
					t.tp = ERR;
					return t;
				}
				cp++;
			}
			continue;
		}
		if(wholeFile[cp] == '\n'){
			cp++;
			linenumber++;
			continue;
		}
		//gets rid of whitespace
		if(wholeFile[cp] == ' ' || wholeFile[cp] == '\r' || wholeFile[cp] == '\t'){
			cp++;
			continue;
		}
		//creates the token
		if (wholeFile[cp] != ' ' && wholeFile[cp] != '\n'){
			//checks for EOF
			if(wholeFile[cp] == EOF){
				strcpy(t.lx,"End of file");
				t.tp = EOFile;
				t.ln = linenumber;
				return t;
			}
			//checks for string literal
			if(wholeFile[cp] == '"'){
				cp++;
				while(wholeFile[cp] != '"'){
					if(wholeFile[cp] == EOF){
						t.ec = EofInStr;
						strcpy(t.lx, "Error: unexpected eof in string constant");
						t.ln = linenumber;
						t.tp = ERR;
						return t;
					}
					if(wholeFile[cp] == '\n' || wholeFile[cp] == '\r'){
						t.ec = NewLnInStr;
						strcpy(t.lx, "Error: new line in string constant");
						t.ln = linenumber;
						t.tp = ERR;
						return t;
					}
					t.lx[tc] = wholeFile[cp];
					tc++;
					cp++;
				}
				cp++;
				t.tp = STRING;
				t.ln = linenumber;
				return t;
			}
			// ID or RESWORD
			if(isalpha(wholeFile[cp]) != 0 || wholeFile[cp] == '_'){
				while(isalpha(wholeFile[cp]) != 0 || isdigit(wholeFile[cp]) != 0 || wholeFile[cp] == '_'){
					t.lx[tc] = wholeFile[cp];
					tc++;
					cp++;
				}
				t.tp = ID;
				for(int i = 0;i < 21;i++){
					if(strcmp(t.lx,reswords[i]) == 0){
						t.tp = RESWORD;
					}
				}
				t.ln = linenumber;
				return t;
			}
			// INT
			if(isdigit(wholeFile[cp]) != 0){
				while(isdigit(wholeFile[cp]) != 0){
					t.tp = INT;
					t.lx[tc] = wholeFile[cp];
					tc++;
					cp++;
				}
				t.ln = linenumber;
				return t;
			}
			// SYMBOL
			if(wholeFile[cp] != '\r' && wholeFile[cp] != '\t'){
				for(int i = 0;i < 21;i++){
					if(wholeFile[cp] == Symbols[i]){
						t.tp = SYMBOL;
					}
				}
				t.lx[tc] = wholeFile[cp];
				t.ln = linenumber;
				if(t.tp != SYMBOL){
					t.tp = ERR;
					t.ec = IllSym;
					strcpy(t.lx, "Error: illegal symbol in source file");
				}
			}
			cp++;
			return t;
		}
	}
	return t;
}

// peek (look) at the next token in the source file without removing it from the stream
Token PeekNextToken ()
{
	Token t;
	int tokenComplete = 0;
	int tc = 0;
	int peekcp = cp;
	int peekln = linenumber;
	while(tokenComplete == 0){
		strcpy(t.fl,global_file_name);
		//clear the tokens string
		memset(t.lx,0,128);
		//checks for /*
		if(wholeFile[peekcp] == '/' && wholeFile[peekcp + 1] == '*'){
			while (wholeFile[peekcp] != '*' || wholeFile[peekcp + 1] != '/'){
				if(wholeFile[peekcp] == EOF){
					t.ec = EofInCom;
					strcpy(t.lx,"Error: unexpected eof in comment");
					t.ln = peekln;
					t.tp = ERR;
					return t;
				}
				if(wholeFile[peekcp] == '\n'){
					peekln++;
				}
				peekcp++;
			}
			peekcp++;peekcp++;
			continue;
		}
		//checks for //
		if(wholeFile[peekcp] == '/' && wholeFile[peekcp + 1] == '/'){
			while(wholeFile[peekcp] != '\n'){
				if(wholeFile[peekcp] == EOF){
					t.ec = EofInCom;
					strcpy(t.lx,"Error: unexpected eof in comment");
					t.ln = peekln;
					t.tp = ERR;
					return t;
				}
				peekcp++;
			}
			continue;
		}
		if(wholeFile[peekcp] == '\n'){
			peekcp++;
			peekln++;
			continue;
		}
		//gets rid of whitespace
		if(wholeFile[peekcp] == ' ' || wholeFile[peekcp] == '\r' || wholeFile[peekcp] == '\t'){
			peekcp++;
			continue;
		}
		//creates the token
		if (wholeFile[peekcp] != ' ' && wholeFile[peekcp] != '\n'){
			//checks for EOF
			if(wholeFile[peekcp] == EOF){
				strcpy(t.lx,"End of file");
				t.tp = EOFile;
				t.ln = peekln;
				return t;
			}
			//checks for string literal
			if(wholeFile[peekcp] == '"'){
				peekcp++;
				while(wholeFile[peekcp] != '"'){
					if(wholeFile[peekcp] == EOF){
						t.ec = EofInStr;
						strcpy(t.lx, "Error: unexpected eof in string constant");
						t.ln = peekln;
						t.tp = ERR;
						return t;
					}
					if(wholeFile[peekcp] == '\n' || wholeFile[peekcp] == '\r'){
						t.ec = NewLnInStr;
						strcpy(t.lx, "Error: new line in string constant");
						t.ln = peekln;
						t.tp = ERR;
						return t;
					}
					t.lx[tc] = wholeFile[peekcp];
					tc++;
					peekcp++;
				}
				peekcp++;
				t.tp = STRING;
				t.ln = peekln;
				return t;
			}
			// ID or RESWORD
			if(isalpha(wholeFile[peekcp]) != 0 || wholeFile[peekcp] == '_'){
				while(isalpha(wholeFile[peekcp]) != 0 || isdigit(wholeFile[peekcp]) != 0 || wholeFile[peekcp] == '_'){
					t.lx[tc] = wholeFile[peekcp];
					tc++;
					peekcp++;
				}
				t.tp = ID;
				for(int i = 0;i < 21;i++){
					if(strcmp(t.lx,reswords[i]) == 0){
						t.tp = RESWORD;
					}
				}
				t.ln = peekln;
				return t;
			}
			// INT
			if(isdigit(wholeFile[peekcp]) != 0){
				while(isdigit(wholeFile[peekcp]) != 0){
					t.tp = INT;
					t.lx[tc] = wholeFile[peekcp];
					tc++;
					peekcp++;
				}
				t.ln = peekln;
				return t;
			}
			// SYMBOL
			if(wholeFile[peekcp] != '\r' && wholeFile[peekcp] != '\t'){
				for(int i = 0;i < 21;i++){
					if(wholeFile[peekcp] == Symbols[i]){
						t.tp = SYMBOL;
					}
				}
				t.lx[tc] = wholeFile[peekcp];
				t.ln = peekln;
				if(t.tp != SYMBOL){
					t.tp = ERR;
					t.ec = IllSym;
					strcpy(t.lx, "Error: illegal symbol in source file");
				}
			}
			peekcp++;
			return t;
		}
	}
	return t;
}

// clean out at end, e.g. close files, free memory, ... etc
int StopLexer ()
{
	fclose(fp);
	return 0;
}

// do not remove the next line
#ifndef TEST
// int main ()
// {
// 	// implement your main function here
// 	// NOTE: the autograder will not use your main function
// 	char* file_name = "Main.jack";
// 	int init = InitLexer(file_name);
// 	Token token;
// 	char datatype[32] = "";
// 	while(strcmp(token.lx,"End of file") != 0){
// 		token = GetNextToken();
// 		if(token.tp == 0){
// 			strcpy(datatype,"RESWORD");
// 		}
// 		else if(token.tp == 1){
// 			strcpy(datatype,"ID");
// 		}
// 		else if(token.tp == 2){
// 			strcpy(datatype,"INT");
// 		}
// 		else if(token.tp == 3){
// 			strcpy(datatype,"SYMBOL");
// 		}
// 		else if(token.tp == 4){
// 			strcpy(datatype,"STRING");
// 		}
// 		else if(token.tp == 5){
// 			strcpy(datatype,"EOFile");
// 		}
// 		else {
// 			strcpy(datatype,"ERR");
// 		}
// 		printf("< %s, %d, %s, %s  >\n",token.fl,token.ln,token.lx,datatype);
// 		//if(token.lx[0] != '\0') {printf("\n< %s, %d, %d, %s  >",token.fl,token.ln,token.ec,datatype);}
// 	}
// 	StopLexer();
// 	return 0;
// }
// do not remove the next line
#endif
