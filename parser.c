#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lexer.h"
#include "parser.h"

ParserInfo pi;
int errorFound;
Token next_token;
Token peek_token;

int type(){
	//printf("type\n");
	if(strcmp(next_token.lx,"int") == 0 || strcmp(next_token.lx,"string") == 0 || strcmp(next_token.lx,"boolean") == 0 || strcmp(next_token.lx,"char") == 0 || next_token.tp == 1){
		return 1;
		// passes so do nothing
	}
	else{
		if(errorFound == 0){
			pi.er = illegalType;
			pi.tk = next_token;
			errorFound = 1;
		}
		return 0;
	}
}

void expression();
void expressionList();
void statement();

void operand(){
	//printf("operand\n");
	//printf("start of operand: %s\n", next_token.lx);
	if(next_token.tp == 2){
		// INTEGER CONSTANT
	}
	else if(next_token.tp == 1){	// IDENTIFIER
		////printf("here with: %s", next_token.lx);
		peek_token = PeekNextToken();
		if(strcmp(peek_token.lx,".") == 0){
			GetNextToken();
			next_token = GetNextToken();
			if(next_token.tp == 1){

			}
			else{
				if(errorFound == 0){
					pi.er = idExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		peek_token = PeekNextToken();
		if(strcmp(peek_token.lx,"[") == 0){
			GetNextToken();
			next_token = GetNextToken();
			expression();
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"]") == 0){

			}
			else{
				if(errorFound == 0){
					pi.er = closeBracketExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		if(strcmp(peek_token.lx,"(") == 0){
			GetNextToken();
			next_token = GetNextToken();
			if(strcmp(next_token.lx,")") != 0){
				expressionList();
				next_token = GetNextToken();
				if(strcmp(next_token.lx,")") == 0){

				}
				else{
					if(errorFound == 0){
						pi.er = closeParenExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			else{
				//printf("empty expressionList");
			}
		}
	}
	else if(strcmp(next_token.lx,"(") == 0){
		next_token = GetNextToken();
		expression();
		next_token = GetNextToken();
		if(strcmp(next_token.lx,")") == 0){

		}
		else{
			if(errorFound == 0){
				pi.er = closeParenExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else if(next_token.tp == 4 || strcmp(next_token.lx,"true") == 0 || strcmp(next_token.lx,"false") == 0 || strcmp(next_token.lx,"null") == 0 || strcmp(next_token.lx,"this") == 0){	// STRING LITERAL, TRUE, FALSE, NULL , THIS
		////printf("...");
	}
	else{
		if(errorFound == 0){
			pi.er = syntaxError;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void factor(){
	//printf("factor\n");
	if(strcmp(next_token.lx,"-") == 0 || strcmp(next_token.lx,"~") == 0){
		next_token = GetNextToken();
	}
	operand();
}

void term(){
	//printf("term\n");
	//next_token = GetNextToken();
	factor();
	peek_token = PeekNextToken();
	while(strcmp(peek_token.lx,"*") == 0 || strcmp(peek_token.lx,"/") == 0){
		GetNextToken();
		next_token = GetNextToken();
		factor();
		peek_token = PeekNextToken();
	}
}

void ArithmeticExpression(){
	//printf("ArithmeticExpression\n");
	//next_token = GetNextToken();
	term();
	peek_token = PeekNextToken();
	while(strcmp(peek_token.lx,"+") == 0 || strcmp(peek_token.lx,"-") == 0){
		GetNextToken();
		next_token = GetNextToken();
		term();
		peek_token = PeekNextToken();
	}
}

void relationalExpression(){
	//printf("relationalExpression\n");
	//next_token = GetNextToken();
	ArithmeticExpression();
	peek_token = PeekNextToken();
	while(strcmp(peek_token.lx,"=") == 0 || strcmp(peek_token.lx,">") == 0 || strcmp(peek_token.lx,"<") == 0){
		GetNextToken();
		next_token = GetNextToken();
		ArithmeticExpression();
		peek_token = PeekNextToken();
	}
}

void expression(){
	//printf("expression\n");
	//printf("start of expression: %s\n", next_token.lx);
	relationalExpression();
	peek_token = PeekNextToken();
	while(strcmp(peek_token.lx,"&") == 0 || strcmp(peek_token.lx,"|") == 0){
		GetNextToken();
		next_token = GetNextToken();
		relationalExpression();
		peek_token = PeekNextToken();
	}
}

void expressionList(){
	//printf("expressionList\n");
	//printf("start of expressionList: %s\n",next_token.lx);
	peek_token = PeekNextToken();
	if(strcmp(peek_token.lx, ")") != 0){
		expression();
		peek_token = PeekNextToken();	//checks for a comma
		while(strcmp(peek_token.lx,",") == 0){
			GetNextToken();
			next_token = GetNextToken();
			expression();
			peek_token = PeekNextToken();
		}
	}
	else{
		//printf("empty expression list\n");
	}
}

void subroutineCall(){
	//printf("subroutineCall\n");
	if(next_token.tp == 1){
		peek_token = PeekNextToken();
		if(strcmp(peek_token.lx,".") == 0){
			GetNextToken();
			next_token = GetNextToken();
			if(next_token.tp == 1){

			}
			else{
				if(errorFound == 0){
					pi.er = idExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		next_token = GetNextToken();
		if(strcmp(next_token.lx,"(") == 0){
			next_token = GetNextToken();
			if(strcmp(next_token.lx,")") != 0){
				expressionList();
				next_token = GetNextToken();
			}
			if(strcmp(next_token.lx,")") == 0){

			}
			else{
				if(errorFound == 0){
					pi.er = closeParenExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = openParenExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = idExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void varDeclarStatement(){
	//printf("varDeclarStatement\n");
	next_token = GetNextToken();
	////printf("\nGoing into type: %s\n",next_token.lx);
	if(type() == 1){
		////printf("\nHere: %s\n",next_token.lx);
		next_token = GetNextToken();
		if(next_token.tp == 1){		//checks for id - has to be an id
			peek_token = PeekNextToken();	//checks for a comma
			while(strcmp(peek_token.lx,",") == 0){
				GetNextToken();
				next_token = GetNextToken();
				if(next_token.tp == 1){
					peek_token = PeekNextToken();
				}
				else{
					if(errorFound == 0){
						pi.er = idExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			next_token = GetNextToken();
			if(strcmp(next_token.lx,";") == 0){

			}
			else{
				if(errorFound == 0){
					pi.er = semicolonExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = idExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
}

//letStatemnt -> let identifier [ [ expression ] ] = expression ;
void letStatement(){
	//printf("letStatement\n");
	next_token = GetNextToken();
	if(next_token.tp == 1){
		peek_token = PeekNextToken();
		if(strcmp(peek_token.lx,"[") == 0){
			GetNextToken();
			next_token = GetNextToken();
			expression();
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"]") == 0){

			}
			else{
				if(errorFound == 0){
					pi.er = closeBracketExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		next_token = GetNextToken();
		if(strcmp(next_token.lx,"=") == 0){
			next_token = GetNextToken();
			if(strcmp(next_token.lx,";") == 0){
				pi.er = syntaxError;
				pi.tk = next_token;
				errorFound = 1;
			}
			expression();
			next_token = GetNextToken();
			if(strcmp(next_token.lx,";") == 0){
				if(strcmp(next_token.lx,";") == 0){

				}
				else{
					if(errorFound == 0){
						pi.er = semicolonExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			else{
				if(errorFound == 0){
					pi.er = semicolonExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = equalExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
}

void ifStatement(){
	//printf("ifStatement\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"(") == 0){
		next_token = GetNextToken();
		expression();
		next_token = GetNextToken();
		if(strcmp(next_token.lx,")") == 0){
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"{") == 0){
				//zero or more statements
				peek_token = PeekNextToken();
				while(strcmp(peek_token.lx,"}") != 0){
					statement();
					peek_token = PeekNextToken();
				}
				next_token = GetNextToken();
				if(strcmp(next_token.lx,"}") == 0){
					peek_token = PeekNextToken();
					if(strcmp(peek_token.lx,"else") == 0){
						GetNextToken();
						next_token = GetNextToken();
						if(strcmp(next_token.lx,"{") == 0){
							peek_token = PeekNextToken();
							while(strcmp(peek_token.lx,"}") != 0){
								statement();
								peek_token = PeekNextToken();
							}
							next_token = GetNextToken();
							if(strcmp(next_token.lx,"}") == 0){

							}
							else{
								if(errorFound == 0){
									pi.er = closeBraceExpected;
									pi.tk = next_token;
									errorFound = 1;
								}
							}
						}
						else{
							if(errorFound == 0){
								pi.er = openBraceExpected;
								pi.tk = next_token;
								errorFound = 1;
							}
						}
					}
				}
				else{
					if(errorFound == 0){
						pi.er = closeBraceExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			else{
				if(errorFound == 0){
					pi.er = openBraceExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = closeParenExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = openParenExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void whileStatement(){
	//printf("whileStatement\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"(") == 0){
		next_token = GetNextToken();
		expression();
		next_token = GetNextToken();
		if(strcmp(next_token.lx,")") == 0){
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"{") == 0){
				peek_token = PeekNextToken();
				while(strcmp(peek_token.lx,"}") != 0){
					statement();
					peek_token = PeekNextToken();
				}
				next_token = GetNextToken();
				if(strcmp(next_token.lx,"}") == 0){

				}
				else{
					if(errorFound == 0){
						pi.er = closeBraceExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			else{
				if(errorFound == 0){
					pi.er = openBraceExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = closeParenExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = openParenExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void doStatement(){
	//printf("doStatement\n");
	next_token = GetNextToken();
	subroutineCall();
	next_token = GetNextToken();
	if(strcmp(next_token.lx,";") == 0){

	}
	else{
		if(errorFound == 0){
			pi.er = semicolonExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void returnStatement(){
	//printf("returnStatement\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,";") != 0 && strcmp(next_token.lx,"}") != 0){
		expression();
		next_token = GetNextToken();
	}
	if(strcmp(next_token.lx,";") == 0){
		next_token = GetNextToken();
	}
	else{
		if(errorFound == 0){
			pi.er = semicolonExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void statement(){
	//printf("statement\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"var") == 0){
		varDeclarStatement();
	}
	else if(strcmp(next_token.lx,"let") == 0){
		letStatement();
	}
	else if(strcmp(next_token.lx,"if") == 0){
		ifStatement();
	}
	else if(strcmp(next_token.lx,"while") == 0){
		whileStatement();
	}
	else if(strcmp(next_token.lx,"do") == 0){
		doStatement();
	}
	else if(strcmp(next_token.lx,"return") == 0){
		returnStatement();
	}
	else{
		if(errorFound == 0){
			pi.er = syntaxError;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void subroutineBody(){
	//printf("subroutineBody\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"{") == 0){
		peek_token = PeekNextToken();
		while(strcmp(peek_token.lx,"var") == 0 || strcmp(peek_token.lx,"let") == 0 || strcmp(peek_token.lx,"if") == 0 || strcmp(peek_token.lx,"while") == 0 || strcmp(peek_token.lx,"do") == 0 || strcmp(peek_token.lx,"return") == 0){
			statement();
			peek_token = PeekNextToken();
		}
		if(strcmp(next_token.lx,"}") == 0){
		}
		else{
			if(errorFound == 0){
				pi.er = closeBraceExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = openBraceExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

void paramList(){
	//printf("paramList\n");
	peek_token = PeekNextToken();
	if(strcmp(peek_token.lx,")") == 0){

	}
	else{
		next_token = GetNextToken();
		if(strcmp(next_token.lx,"{") == 0){
			if(errorFound == 0){
				pi.er = closeParenExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
		if(type() == 1){
			next_token = GetNextToken();
			if(next_token.tp == 1){
				peek_token = PeekNextToken();
				while(strcmp(peek_token.lx,",") == 0){
					GetNextToken();
					next_token = GetNextToken();
					type();
					next_token = GetNextToken();
					if(next_token.tp == 1){

					}
					else{
						if(errorFound == 0){
							pi.er = idExpected;
							pi.tk = next_token;
							errorFound = 1;
						}
					}
					peek_token = PeekNextToken();
				}
			}
			else{
				if(errorFound == 0){
					pi.er = idExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
	}
}

void subroutineDeclar(){
	//printf("subroutineDeclar\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"void") == 0 || type() == 1){
		next_token = GetNextToken();
		if(next_token.tp == 1){
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"(") == 0){
				paramList();
				next_token = GetNextToken();
				if(strcmp(next_token.lx,")") == 0){
					subroutineBody();
				}
				else{
					if(errorFound == 0){
						pi.er = closeParenExpected;
						pi.tk = next_token;
						errorFound = 1;
					}
				}
			}
			else{
				if(errorFound == 0){
					pi.er = openParenExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = idExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
}

void classVarDeclar(){
	//printf("classVarDeclar\n");
	next_token = GetNextToken();
	type();
	next_token = GetNextToken();
	if(next_token.tp == 1){
		peek_token = PeekNextToken();
		while(strcmp(peek_token.lx,",") == 0){
			GetNextToken();
			next_token = GetNextToken();
			if(next_token.tp == 1){

			}
			else{
				if(errorFound == 0){
					pi.er = idExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
			peek_token = PeekNextToken();
		}
		next_token = GetNextToken();
		if(strcmp(next_token.lx,";") == 0){

		}
		else{
			if(errorFound == 0){
				pi.er = semicolonExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = idExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

// you can declare prototypes of parser functions below
void memberDeclar(){
	////printf("memberDeclar\n");
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"constructor") == 0 || strcmp(next_token.lx,"function") == 0 || strcmp(next_token.lx,"method") == 0 || strcmp(next_token.lx,"static") == 0 || strcmp(next_token.lx,"field") == 0){
		if(strcmp(next_token.lx,"constructor") == 0 || strcmp(next_token.lx,"function") == 0 || strcmp(next_token.lx,"method") == 0){
			subroutineDeclar();
		}
		else{
			classVarDeclar();
		}
	}
	else{
		if(errorFound == 0){
			pi.er = classVarErr;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}


void classDeclar(){
	//printf("classDeclar\n");
	next_token = GetNextToken();
	if(next_token.tp == 1){
		next_token = GetNextToken();
		if(strcmp(next_token.lx,"{") == 0){
			peek_token = PeekNextToken();
			while(strcmp(peek_token.lx,"}") != 0 && strcmp(peek_token.lx,"End of file") != 0){
				memberDeclar();
				peek_token = PeekNextToken();
			}
			next_token = GetNextToken();
			if(strcmp(next_token.lx,"}") == 0){

			}
			else{
				if(errorFound == 0){
					pi.er = closeBraceExpected;
					pi.tk = next_token;
					errorFound = 1;
				}
			}
		}
		else{
			if(errorFound == 0){
				pi.er = openBraceExpected;
				pi.tk = next_token;
				errorFound = 1;
			}
		}
	}
	else{
		if(errorFound == 0){
			pi.er = idExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
}

int InitParser (char* file_name)
{
	InitLexer(file_name);
	return 1;
}

ParserInfo Parse ()
{
	errorFound = 0;
	next_token = GetNextToken();
	if(strcmp(next_token.lx,"class") == 0){
		classDeclar();
	}
	else{
		if(errorFound == 0){
			pi.er = classExpected;
			pi.tk = next_token;
			errorFound = 1;
		}
	}
	if(pi.tk.ec == 1){
		pi.er = lexerErr;
	}
	return pi;
}

int StopParser ()
{
	StopLexer();
	return 1;
}

#ifndef TEST_PARSER
int main ()
{
	errorFound = 0;
	char* file_name = "syntaxError1.jack";
	InitParser(file_name);
	ParserInfo i = Parse();
	StopParser();
	//printf("\nError code: %u\n", pi.er);
	//printf("Next token: %s\n", next_token.lx);
	return 0;
}
#endif
